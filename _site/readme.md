# WEBVISÃO

Meu blog pessoal abordando minhas experiencias, conhecimentos, e alguns momentos de aborrecimentos (até rima)

## RE-DESIGN

A nova interface do blog possui um conceito minimalista, focando-se totalmente para o conteúdo.

## BLOG ENGINE

O jekyll é a nova engine do blog substituindo o wordpress que não era mais necessario devio o tamanho do blog, performance e segurança (não há nada para invadir).

Para poder iniciar o blog localmente você precisa:
1. Faça um fork no repósitorio e depois clone para a sua maquina.

2. Ter o [Ruby](ruby-lang.org/pt/ "ruby") instalado.

3. Caso não tiver o jekyll instalado abra a linha de comandos do ruby e digite: 

   gem install jekyll

4. Ainda na linha de comandos do ruby navegue até ao root do projecto cd nome da pasta depois é só inserir o comando:

   jekyll